const assert = require('assert');

Feature('Locale Number Formatting');

Before(login => {
  login('admin'); // login using user session
});

Scenario('Check calculations', async I => {
  
  I.say('I am going to view the Number Locale Calculation Test Post');
  I.amOnPage('http://s30151.p100.sites.pressdns.com/wp-admin/edit.php?post_type=page');
  I.executeScript(function() {
    document.querySelector('a[aria-label="View “Number Locale Calculation Test”"]').click();
  })

  let testCases = [
    { index: 0, number: '10', negativeNumber: '-5', formattedNumber: '5.50', expectedNumTimesTwo: '20.00', expectedNumPlusNegNum: '5.00' },
    { index: 1, number: '12', negativeNumber: '-8', formattedNumber: '3.75', expectedNumTimesTwo: '24.00', expectedNumPlusNegNum: '4.00' },
  ]

  // In CodeceptJS, this type of loop is acceptable, but .forEach() is not
  for (set of testCases) {

      I.say(`\nTrying number set ${set.index}`);
    
      I.scrollTo({ css: '.js-number' });
    
      I.say('I am filling in the number field');
      I.waitForElement('.js-number');
      I.fillField('.js-number', set.number);
      
      I.say('I am filling in the negative number field');
      I.waitForElement('.js-negative-number');
      I.fillField('.js-negative-number', set.negativeNumber);
      
      I.say('I am filling in the formatted number field');
      I.waitForElement('.js-formatted-number');
      I.fillField('.js-formatted-number', set.formattedNumber);
    
      I.say('I am checking number times two');
      let numberTimesTwo = await I.grabTextFrom({ css: 'span[data-key="calc:number_times_2"]' });
      assert.equal(set.expectedNumTimesTwo, numberTimesTwo);
      
      I.say('I am checking number plus negative number');
      let numberPlusNegativeNumber = await I.grabTextFrom({ css: 'span[data-key="calc:number_plus_negative_number"]' });
      assert.equal(set.expectedNumPlusNegNum, numberPlusNegativeNumber);
      
      /**
       * The following two blocks should be re-applied when number formatting is supported
       */
    
      // I.say('I am checking number plus formatted number');
      // let numberPlusFormattedNumber = await I.grabTextFrom({ css: 'span[data-key="calc:number_plus_formatted_number"]' });
      // assert.equal('13.50', numberPlusFormattedNumber);
      
      // I.say('I am checking number plus list calc value');
      // let numberPlusListCalcValue= await I.grabTextFrom({ css: 'span[data-key="calc:number_plus_negative_number"]' });
      // assert.equal('5.00', numberPlusListCalcValue);
    
      I.scrollTo({ css: '#nf-field-69' })
  }

  I.say('\nI am submitting the form');
  I.click({ css: '#nf-field-69' });
  
})