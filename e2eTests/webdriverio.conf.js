const { username, password } = require('./secrets.js');

exports.config = {
  tests: './active/*.test.js',
  output: './output',
  timeout: 10000,
  helpers: {
    WebDriver: {
      url: 'http://js-acceptance:5000',
      host: process.env.HOST, // alternatively you could not use an env varialble and simply
      browser: 'chrome', // use the name of the container directly, 'selenium-chrome-container'
      smartWait: 5000,
      waitForTimeout: 10000,
      desiredCapabilities: {},
    },
  },
  plugins: {
    autoLogin: { // This object is what allows us to inject credentials to each scenario
      enabled: true,
      // saveToFile: true,
      inject: 'login',
      users: {
        admin: {
          login: (I) => {
            // Login in Via the Admin Page
            I.say('I am going to Login Via the Admin Page');
            I.amOnPage('http://s30151.p100.sites.pressdns.com/wp-login.php');
            I.waitForText('Username or Email Address');
            I.waitForText('Password');
  
            // Submit the login form
            I.say('I am going to submit the login form');
            within('#loginform', () => {
              I.fillField('log', username);
              I.fillField('pwd', password);
              I.click('#wp-submit');
            });
            I.waitForText('Dashboard');
          },
          check: (I) => { // This is what tells us that we're logged in
            I.dontSee('#loginform');
          },
          fetch: () => {}, // empty function
          restore: () => {}, // empty funciton
        }
      },
    }
  }
};
