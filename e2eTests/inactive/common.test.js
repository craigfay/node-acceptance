Feature('Form Publication/Deletion');

Before(login => {
  login('admin'); // login using user session
});

Scenario('Create a Ninja Form', I => {

  I.amOnPage('http://s30151.p100.sites.pressdns.com/wp-admin');

  // Ninja Forms Dashboard
  I.say('I am going to view the Ninja Forms Dashboard');
  I.click('Ninja Forms');
  I.waitForText('ADD NEW', '.add');
  
  // Create a new form
  I.say('I am going to create a form');
  I.click('Add New', '.add'); // I'm not sure why this can't match the I.waitForText() above
  I.waitForText('Blank Form')
  I.click('Blank Form');
  
  // NF Builder
  I.say('I am going to use the Form Builder');
  I.waitForText('Add form fields', 'h3');
  I.waitForText('Get started by adding your first form field. It\'s that easy.', 'p');
  
  I.say('I am going to search for a field type');
  I.waitForElement('.nf-filter');
  I.appendField('.nf-filter', 'n');
  I.appendField('.nf-filter', 'u');
  I.appendField('.nf-filter', 'm');
  I.waitForText('Number', '.nf-item');
  I.waitForText('Number', '.nf-item')
  
  I.say('I am going to add a number field');
  I.click('Number', '.nf-item');
  I.waitForText('Number', '.nf-field-label');
  
  I.say('I am going to close the drawer');
  I.click('Done', '.nf-close-drawer');
  I.waitForText('PUBLISH', '.publish');
  I.click('PUBLISH', '.publish');
  
  I.say('I am going to name the form');
  I.waitForElement('#title');
  I.appendField('#title', 'Locale Test');
  
  I.say('I am going to publish the form');
  I.waitForText('PUBLISH', '.nf-close-drawer');
  I.click('Publish');
  
  // For reasons unknown, the test will stall if this last step is
  I.waitForElement('.fa-times');
  I.click('.fa-times');
});

Scenario('Put a form on a page', I => {
  I.say('I am going to create a new page');
  I.amOnPage('http://s30151.p100.sites.pressdns.com/wp-admin/post-new.php?post_type=page');
  
  
  I.say('I am going to append a Ninja Form');
  I.waitForElement('#nf_admin_metaboxes_appendaform');
  I.click('#nf_admin_metaboxes_appendaform');
  
  I.waitForElement({ name: 'ninja_form_select'});
  I.click({ name: 'ninja_form_select'});
  
  I.selectOption('ninja_form_select', 'Locale Test');

  I.waitForElement('#post-title-0');
  I.fillField('#post-title-0', 'NF Locale Test');
  
 I.say('I am going to publish the page') 
  I.waitForElement({ css: 'button.is-primary' });
  I.click({ css: 'button.is-primary' });
 
  I.waitForElement({ css: 'button.is-large' });
  I.click({ css: 'button.is-large' });
})

Scenario('Delete A Form', I => {
  
  I.amOnPage('http://s30151.p100.sites.pressdns.com/wp-admin');

  // Ninja Forms Dashboard
  I.say('I am going to view the Ninja Forms Dashboard');
  I.click('Ninja Forms');
  I.waitForElement('.nf-edit-settings');
  I.click('.nf-edit-settings');
  
  I.waitForText('Delete', '.delete');
  I.click('Delete', '.delete');
  
  I.waitForElement('#confirmDeleteFormInput');
  I.fillField('#confirmDeleteFormInput', 'DELETE');
  
  I.click('.confirm');
})


